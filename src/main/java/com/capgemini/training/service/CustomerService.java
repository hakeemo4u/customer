package com.capgemini.training.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.training.customer.entity.Address;
import com.capgemini.training.customer.entity.Customer;
import com.capgemini.training.repository.AddressRepository;
import com.capgemini.training.repository.CustomerRepository;


@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AddressRepository addressRepository;
	
	
	@Transactional
	public void save(Customer cust) {

		customerRepository.save(cust);
	}

	@Transactional
	public void delete(Long id) {
		customerRepository.deleteById(id);
	}
	
	@Transactional
	public void update(Long id) {
		Optional<Customer> customerList = customerRepository.findById(id);
		Customer customer = null ;
		if (customerList.isPresent()) {
			customer = customerList.get();
			customer.setFirstName("firstNameOne");
		}
		customerRepository.save(customer);
	}
	
	public List<Customer> getAllCustomer() {
		List<Customer> customer = new ArrayList<>();
		for (Customer cust : customerRepository.findAll()) {
			customer.add(cust);
		}
		return customer;
	}
	
	
	public Customer getCustomer(Long id) {
		return customerRepository.findById(id).get();
	}
	
	
	public List<Address> getCustomerAddress(Long id)
	{
		return customerRepository.findById(id).get().getAddress();
	}
	
	
	public void updateAddress( Long id)
	{	Optional<Address> addressList = addressRepository.findById(id);
	
	Address address=null;
	if (addressList.isPresent()) {
		address = addressList.get();
		address.setFirstName("Sakshi");
	}
	addressRepository.save(address);
	
	}
	
	
	public Address getCustomerAdressById(Long Cid,Long Aid)
	{
		Address addr=null;
		Customer cust= customerRepository.findById(Cid).get();
		List<Address> addressList=cust.getAddress();
		Address address=addressRepository.findById(Aid).get();
		for(Address addrs : addressList)
		{
			if(addrs.equals(address))
				addr=addrs;
				return addr;
		}
		return addr;
	}

	
	
	public Customer updateAddressById( Address addrss,Long cId, Long aId)
	{
		Customer cust=null;
		Customer cust1= customerRepository.findById(cId).get();
		List<Address> addressList=cust1.getAddress();
		Address address=addressRepository.findById(aId).get();
		for(Address addrs : addressList)
		{
			if(addrs.equals(address))
			{
				address.setFirstName(addrss.getFirstName());
				List<Address> list=new ArrayList<>();
				list.add(address);
				cust1.setAddress(list);
			}
				
		}
		return cust1;
		//return customerRepository.findByEntityIdAndEntityIds(cId, aId);
	}
	
	}
