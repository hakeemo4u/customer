package com.capgemini.training.customer.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Communication {
	
	@Id
	@GeneratedValue
	private Long leadId;
	private Long customerId;
	private String email;
	private String communicationType;
	private String communicationChannel;
	private String sourceSystem;
	private Timestamp creationDate;
	private String permissionCaptureMethod;
	private Long permissionStatus;
	private Timestamp permissionDate;
	private String countryCode;
	private String languageCode;
	public Long getLeadId() {
		return leadId;
	}
	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCommunicationType() {
		return communicationType;
	}
	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}
	public String getCommunicationChannel() {
		return communicationChannel;
	}
	public void setCommunicationChannel(String communicationChannel) {
		this.communicationChannel = communicationChannel;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public String getPermissionCaptureMethod() {
		return permissionCaptureMethod;
	}
	public void setPermissionCaptureMethod(String permissionCaptureMethod) {
		this.permissionCaptureMethod = permissionCaptureMethod;
	}
	public Long getPermissionStatus() {
		return permissionStatus;
	}
	public void setPermissionStatus(Long permissionStatus) {
		this.permissionStatus = permissionStatus;
	}
	public Timestamp getPermissionDate() {
		return permissionDate;
	}
	public void setPermissionDate(Timestamp permissionDate) {
		this.permissionDate = permissionDate;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	
}
